## 总览

| 功功率率名称    | 型号     | 能力             | 用电      | 功率KW  |
| --------------- | -------- | ---------------- | --------- | ------- |
| 包子机          | XZ-612   | 1800个/小时      | 220V/380V | 1.55    |
| **饺子机 ?**    | XZ-160   | 约10秒/条        | 220V/380V | 2.2/1.5 |
| **双速和面机?** | XZH-20   | 230/120 rpm      | 220V      | 1.1     |
| 自动压面机      | YMZD350A | 700kg/hour       | 220V      | 2.2     |
| 绞肉机          | XZ-12A   | 120kg/hour       | 220V      | 0.65    |
| **搅拌机?**     | XZM-20   | 360/166/100 rpm  | 220V/380V | 1.1     |
| 刹菜机          | XZ-400   | 400kg/hour       | 220V      | 0.37    |
| **醒发机?**     | WFX-12E  | 未查到           | 未查到    | 未查到  |
| **蒸饭车?**     | CH-150   | 未查到           | 未查到    | 未查到  |
| 切菜机          | CHD-20   | 120-350千克/小时 | 220/380V  | 0.75    |
| 电饼铛          | VD-500   | 45kg/hour        | 220/380V  | 4       |



## [**SZ-612型全自动包子机**](http://www.yamaca.com/index.php/baozijixilie/812.html)



![image-20210731140106948](picture/image-20210731140106948.png)

![image-20210731140149424](picture/image-20210731140149424.png)

## [XZ-160 开背机 ?](http://www.hzsaili.com/index.php/shayujixilie/868.html)

型号不匹配



![image-20210731140734948](picture/image-20210731140734948.png)

## [SZH-20型双速双动和面机 ?](http://www.yamaca.com/index.php/hemianjijiaobanji/89.html) 

未找到型号XZH-20

![image-20210807221637418](pictures/image-20210807221637418.png)

## [**YMZD-350A自动压面**](http://www.yamaca.com/index.php/yamianjimiantiaoji/831.html)

![image-20210807222050852](pictures/image-20210807222050852.png)



![image-20210807222137528](pictures/image-20210807222137528.png)

## [**SZ-12A 绞肉机**](http://www.yamaca.com/index.php/jiaoroujixilie/227.html)

![image-20210807222252496](pictures/image-20210807222252496.png)

![image-20210807222318115](pictures/image-20210807222318115.png)

## [**SZM-20型四功能搅拌机**?](http://www.yamaca.com/index.php/hemianjijiaobanji/85.html)

![image-20210807222601997](pictures/image-20210807222601997.png)

![image-20210807222610246](pictures/image-20210807222610246.png)

## [**SZ-400刹菜机**](http://www.yamaca.com/index.php/qiesuijixilie/190.html)



![image-20210807222723318](pictures/image-20210807222723318.png)

![image-20210807222741966](pictures/image-20210807222741966.png)

## [**CHD-20型多能切菜机**](http://www.yamaca.com/index.php/qiesiqiepianqiedingji/172.html)

![image-20210807223234138](pictures/image-20210807223234138.png)



## [**VF多层面包醒发箱系列?**](http://www.yamaca.com/index.php/xingfaxianghongkaolu/108.html)

![image-20210807224218819](pictures/image-20210807224218819.png)

## [CH-A-150 **多功能普通蒸饭柜 ?**](http://www.yamaca.com/index.php/zhengfanchezhengbaolu/114.html)

![image-20210807224354133](pictures/image-20210807224354133.png)

##  [VD-500 电饼铛](https://www.spzs.com/showprovideinfo/5273146.html) 



型号 VD-500
电压 220/380V
额定频率 50HZ
功率 4KW   
温度范围 50~300OC   
外形尺寸(mm) 600*750*750   
产量(公斤/小时) 45   
重量(公斤) 52   '